/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import proceso.Constantes;
import proceso.DatoLogueo;
import proceso.DatoUsuario;

/**
 *
 * @author Jhon
 */
public class BaseDatosUsuario {
    
    public void crearUsuario (final DatoLogueo datoLogueo) {
        String query = "INSERT INTO persona(documento, nombre, apellido, telefono, correo, direccion, foto, estado) VALUES (?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = Constantes.conexion().prepareStatement(query);
            ps.setString(1, datoLogueo.getDocumento());
            ps.setString(2, datoLogueo.getNombre());
            ps.setString(3, datoLogueo.getApellido());
            ps.setString(4, datoLogueo.getTelefono());
            ps.setString(5, datoLogueo.getCorreo());
            ps.setString(6, datoLogueo.getDireccion());
            ps.setBinaryStream(7,  datoLogueo.getImagen());
            ps.setBoolean(8, true);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(BaseDatosUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        new BaseDatosLogueo().crearLogueo(datoLogueo);
    }

    
    public void editarUsuario(DatoLogueo datosLogueo) {
        String query = "UPDATE persona SET nombre = ? , apellido = ? , telefono = ? ,correo = ? ,direccion = ? ,foto = ? WHERE documento = \"" + datosLogueo.getDocumento() + "\"";
        try {
            PreparedStatement ps = Constantes.conexion().prepareStatement(query);
            ps.setString(1, datosLogueo.getNombre());
            ps.setString(2, datosLogueo.getApellido());
            ps.setString(3, datosLogueo.getTelefono());
            ps.setString(4, datosLogueo.getCorreo());
            ps.setString(5, datosLogueo.getDireccion());
            ps.setBinaryStream(6,  datosLogueo.getImagen());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            Logger.getLogger(BaseDatosUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        new BaseDatosLogueo().editarLogueo(datosLogueo);
    }
    
    public Object[] cargarUsuario (final String docu) {
        Object[] datos = new Object[7];
        String query = "SELECT * FROM persona WHERE documento = \"" + docu + "\"";
        try {
            Statement st = Constantes.conexion().createStatement();
            ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                datos[0] = rs.getString(1); //Documento
                datos[1] = rs.getString(2); //Nombre
                datos[2] = rs.getString(3); //Apellido
                datos[3] = rs.getString(4); //Telefono
                datos[4] = rs.getString(5); //Correo
                datos[5] = rs.getString(6); //Direccion
                Blob blob = rs.getBlob(7);
                if (blob != null) {
                    ImageIO.write(ImageIO.read(blob.getBinaryStream()), "PNG", new File("foto.png"));
                    FileInputStream fis = new FileInputStream("foto.png");
                    datos[6] = fis; //Foto
                }
            }
            st.close();
            rs.close();
        } catch (SQLException | IOException ex) {
            Logger.getLogger(BaseDatosUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*DatoUsuario du = new DatoUsuario();
        String query = "SELECT * FROM persona WHERE documento = \"" + docu + "\"";
        
        try {
            Statement st = Constantes.conexion().createStatement();
            ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                du.setDocumento(rs.getString(1));
                du.setNombre(rs.getString(2));
                du.setApellido(rs.getString(3));
                du.setTelefono(rs.getString(4));
                du.setCorreo(rs.getString(5));
                du.setDireccion(rs.getString(6));
                Blob blob = rs.getBlob(7);
                if (blob != null) {
                    ImageIO.write(ImageIO.read(blob.getBinaryStream()), "PNG", new File("foto.png"));
                    FileInputStream fis = new FileInputStream("foto.png");
                    du.setImagen(fis);
                }
            }
            st.close();
            rs.close();
        } catch (SQLException | IOException ex) {
            Logger.getLogger(BaseDatosUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return du;*/
        return datos;
    }
   
    public boolean identificacionDuplicada(final String docu) {
        boolean duplicado = false;
        String query = "SELECT documento FROM persona WHERE documento = \"" + docu + "\"";
        try {
            Statement st = Constantes.conexion().createStatement();
            ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                duplicado = true;
            }
            st.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(BaseDatosUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return duplicado;
    }

}
