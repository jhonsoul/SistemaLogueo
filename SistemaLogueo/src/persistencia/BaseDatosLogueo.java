/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.FileInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import proceso.Constantes;
import proceso.DatoLogueo;

/**
 *
 * @author Jhon
 */
public class BaseDatosLogueo {
    
        
    public boolean crearLogueo (DatoLogueo dl) {
        String query = "INSERT INTO usuario(usr_id, usr_pass, documento, estado) VALUES (?,?,?,?)";
        boolean dupli = false;
        try {
            PreparedStatement ps = Constantes.conexion().prepareStatement(query);
            ps.setString(1, dl.getUsuario());
            ps.setString(2, dl.getPassword());
            ps.setString(3, dl.getDocumento());
            ps.setBoolean(4, true);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException ex) {
            if (ex.getErrorCode() == 1062) {
                dupli = true;
            }
            System.out.println(ex.getMessage());
        }
        return dupli;
    }
    
    
    public void editarLogueo(DatoLogueo dl) {
        String query = "UPDATE usuario SET usr_id = \"" + dl.getUsuario() + "\", usr_pass = \"" + dl.getPassword() + "\" WHERE documento = \"" + dl.getDocumento()+ "\"";
        try {
            Statement st = Constantes.conexion().createStatement();
            st.executeUpdate(query);
            st.close();
        } catch (SQLException ex) {
            Logger.getLogger(BaseDatosUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public DatoLogueo cargarLogueo (final String docu) {
        
        DatoLogueo datos = new DatoLogueo();
        //Cargo los datos basicos del usuario
        Object [] inf = new BaseDatosUsuario().cargarUsuario(docu);
        datos.setDocumento(String.valueOf(inf[0]));
        datos.setNombre(String.valueOf(inf[1]));
        datos.setApellido(String.valueOf(inf[2]));
        datos.setTelefono(String.valueOf(inf[3]));
        datos.setCorreo(String.valueOf(inf[4]));
        datos.setDireccion(String.valueOf(inf[5]));
        datos.setImagen((FileInputStream)inf[6]);
        String query = "SELECT * FROM usuario WHERE documento = \"" + docu + "\"";
        try {
            Statement st = Constantes.conexion().createStatement();
            ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                datos.setDocumento(rs.getString(3));
                datos.setUsuario(rs.getString(1));
                datos.setPassword(rs.getString(2));
            }
            st.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(BaseDatosUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos;
    }
    
    public boolean logueo (final String id, final String contra) {
        boolean esta = false;
        String query = "SELECT * FROM usuario WHERE usr_id = \"" + id + "\" AND usr_pass = \"" + contra +"\" AND estado = true";
        try {
            Statement st = Constantes.conexion().createStatement();
            ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                esta = true;
            }
            st.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(BaseDatosUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }    
        return esta;
    }
    
    public boolean usuarioDuplicado (final String usuario) {
        boolean duplicado = false;
        String query = "SELECT usr_id FROM usuario WHERE usr_id = \"" + usuario + "\"";
        try {
            Statement st = Constantes.conexion().createStatement();
            ResultSet rs = st.executeQuery(query);
            if  (rs.next()) {
                duplicado = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(BaseDatosLogueo.class.getName()).log(Level.SEVERE, null, ex);
        }
        return duplicado;
    }

}
