/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import proceso.Constantes;

/**
 *
 * @author Jhon
 */
public class BaseDatosDatos {
    
    
    
    public Object[][] llenarDatos () {
        Object datos[][] = null;
        try {
            String query = "SELECT documento, nombre, apellido, telefono, correo, direccion, estado FROM persona";
            PreparedStatement ps = Constantes.conexion().prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            //Para definir el tamaño de la matriz de objetos
            rs.last();
            ResultSetMetaData rsmd = rs.getMetaData();
            int cantidadColumnas = rsmd.getColumnCount();
            int cantidadFilas =  rs.getRow();
            datos = new Object[cantidadFilas][cantidadColumnas];
            rs.beforeFirst();
            int j = 0;
            while (rs.next()) {
                for (int i = 0; i < cantidadColumnas; i++) {
                    datos[j][i] = rs.getString(i + 1);
                }
                j++;
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(BaseDatosDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return datos;
    }
    
    public void cambiarEstadoPersona (final String docu, final boolean es) {
        try {
            String query = "UPDATE persona SET estado = " + es + " WHERE documento = \"" + docu + "\"";
            Statement st = Constantes.conexion().createStatement();
            st.executeUpdate(query);
            st.close();
            query = "UPDATE usuario SET estado = " + es + " WHERE documento = \"" + docu + "\"";
            st = Constantes.conexion().createStatement();
            st.executeUpdate(query);
            st.close();
        } catch (SQLException ex) {
            Logger.getLogger(BaseDatosDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean verificadorInicial () {
        boolean existeDatos = false;
        String query = "SELECT documento FROM persona";
        try {
            Statement st = Constantes.conexion().createStatement();
            ResultSet rs = st.executeQuery(query);
            if (rs.next()) {
                existeDatos = true;
            }
            st.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(BaseDatosUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return existeDatos;
    }
}
