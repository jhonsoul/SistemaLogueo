/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proceso;

/**
 *
 * @author Jhon
 */
public class DatoLogueo extends DatoUsuario{
    
    private String usuario;
    private String password;

    public DatoLogueo(String usuario, String password) {
        this.usuario = usuario;
        this.password = password;
    }
    
    public DatoLogueo() {
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
