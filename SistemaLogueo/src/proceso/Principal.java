/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proceso;

import javax.swing.JOptionPane;
import persistencia.BaseDatosDatos;
import vista.VentanaLogueo;
import vista.VentanaUsuario;


/**
 *
 * @author Jhon
 */
public class Principal {
    public static void main(String[] args){
        //Verifico si existe conexión a la base de datos
        if (Constantes.conexion() == null) {
            JOptionPane.showMessageDialog(null, "No existe conexión con la base de datos", "Error en conexión", JOptionPane.ERROR_MESSAGE);
        } else {
            if (new BaseDatosDatos().verificadorInicial()) {
                //Entra en la ventana de logueo cuando hay informacion en la base de datos 
                new VentanaLogueo().setVisible(true);
            } else {
                //Entra cuando no hay información en la base de datos
                new VentanaUsuario().setVisible(true);
            }
            
        }
    }
}
