/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proceso;

import java.awt.Color;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Jhon
 */
public class Constantes {
    public static final Color COLOR_FONDO = new Color(153, 204, 255);
    
    public static Connection conexion(){
        Connection c = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            c = DriverManager.getConnection("jdbc:mysql://localhost:3306/sistema_logueo", "root", "");
        } catch (ClassNotFoundException | SQLException ex) {
            JOptionPane.showMessageDialog(null, "Se presento un error en conexión " + ex,"Conexión fallida", JOptionPane.ERROR_MESSAGE);
        }
        return c;
    }
}
